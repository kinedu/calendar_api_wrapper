require "thor"
require "calendar_api_wrapper/version"
require "calendar_api_wrapper/configuration"
require "calendar_api_wrapper/service"
require "calendar_api_wrapper/templates/access_token"
require "calendar_api_wrapper/templates/client"
require "calendar_api_wrapper/templates/group"
require "calendar_api_wrapper/templates/user"

module CalendarApiWrapper
  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield configuration
  end

  class Cli < ::Thor
    include Thor::Actions

    def self.source_root
      File.expand_path('templates', __dir__)
    end

    desc 'initializer:create', 'Create initializer file.'

    def create_initializer
      if yes?("do you want to create initializer file?")
        copy_file(
          'initializer.rb',
          'config/initializers/calendar_api_wrapper.rb'
        )
      end
    end
  end

end
