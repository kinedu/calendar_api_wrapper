require "calendar_api_wrapper/templates/template_request"
module CalendarApiWrapper
  class Client < TemplateRequest
      
      def initialize(params)
        @body = params[:body]
        @id   = params[:id]
      end

      def type_resouce
        "clients"
      end

      def url_resource
        type_resouce
      end
      
  end
end
  