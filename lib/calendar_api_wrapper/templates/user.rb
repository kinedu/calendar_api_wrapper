require "calendar_api_wrapper/templates/template_request"
module CalendarApiWrapper
  class User < TemplateRequest

      def initialize(params)
        @id        = params[:id]
        @body      = params[:body]
        @client_id = params[:client_id]
        @query     = params[:query]
      end

      def type_resouce
        "users"
      end

      def url_resource
        "clients/#{client_id}/#{type_resouce}"
      end

      def url_restore
        "#{url_resource_id}/restore"
      end

  end
end
  