require "calendar_api_wrapper/templates/template_request"
module CalendarApiWrapper
  class AccessToken < TemplateRequest
      
      def initialize(params)
        @body = params[:body]
      end

      def type_resouce
        "issue-access-token"
      end

      def url_resource
        type_resouce
      end

      def validate_access_token
        call_service.call({ :body => body, :type_request => "Post", :url_resource => "validate-access-token" }	 )
      end

      def update  ; end 

      def destroy ; end
      
      def show    ; end

      def index   ; end
      
  end
end
  