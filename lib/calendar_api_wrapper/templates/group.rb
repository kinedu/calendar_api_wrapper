require "calendar_api_wrapper/templates/template_request"
module CalendarApiWrapper
  class Group < TemplateRequest

      def initialize(params)
        @id        = params[:id]
        @body      = params[:body]
        @client_id = params[:client_id]
        @query     = params[:query]
      end

      def type_resouce
        "groups"
      end

      def sync_users
        update_group("sync-users")
      end   
      
      def remove_users
        update_group("remove-users")
      end   

      def tag(tag)
        extra(tag)
      end

      def url_resource
        "clients/#{client_id}/#{type_resouce}"
      end

      def url_restore
        "#{url_resource_id}/restore"
      end

      private

      def update_group(type_accion)
        call_service.call({ :body => body, :type_request => "Put", :url_resource =>  "#{url_resource}/#{id}/#{type_accion}" }) 
      end

  end
end
  