require 'net/http'
module CalendarApiWrapper
  class HttpRequest
    attr_accessor :uri, :http, :request, :params, :url_resource, :query

    def initialize(params)
      @url_resource  =  params[:url_resource]
      @query         =  params[:query]
      @uri           =  build_uri
      @http          =  ::Net::HTTP.new(uri.host, uri.port)
      @request       =  "::Net::HTTP::#{params[:type_request]}".constantize.new(build_path, headers)
      request.body   =  params[:body].to_json
      yield self if block_given?
    end

    def start
      http.use_ssl = true
      http.start{ |protocol| protocol.request(request) }
    end

    private

    def build_uri
      URI("#{configurations.api_calendar_url}/#{url_resource}")
    end

    def build_path
      return uri.path.concat("?#{params_encode(query)}") if query && query.instance_of?(Array)
      uri.path
    end

    def params_encode(queries)
      queries.map{ |q| "#{q.first}=#{URI.encode(q.last)}" }.compact.join('&')
    end

    def headers
      { 'X-API-KEY' => configurations.api_key, "Content-Type" => "application/json", 'Accept' => 'application/json'}
    end

    def configurations
      CalendarApiWrapper.configuration
    end
  end
end
