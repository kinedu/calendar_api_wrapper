CalendarApiWrapper.configure do |config|
  config.api_key = ENV['CALENDAR_API_KEY']
  config.api_calendar_url = ENV['CALENDAR_API_URL']
end