RSpec.describe CalendarApiWrapper do
  describe '#configure' do
    before do
      subject.configure do |config|
        config.api_key = Faker::Crypto.md5
        config.api_calendar_url = Faker::Crypto.md5
      end
    end

    it '#api_key value is expected to be a kind of String' do
      expect(subject.configuration.api_key).to be_a(String)
    end

    it '#aws_url value is expected to be a kind of String' do
      expect(subject.configuration.api_calendar_url).to be_a(String)
    end

  end

  it "has a version number" do
    expect(subject::VERSION).not_to be nil
  end
end
